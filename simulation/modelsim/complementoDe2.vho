-- Copyright (C) 1991-2014 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 14.1.0 Build 186 12/03/2014 SJ Web Edition"

-- DATE "05/09/2019 16:05:47"

-- 
-- Device: Altera 5CEBA4F23C7 Package FBGA484
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA_LNSIM;
LIBRARY CYCLONEV;
LIBRARY IEEE;
USE ALTERA_LNSIM.ALTERA_LNSIM_COMPONENTS.ALL;
USE CYCLONEV.CYCLONEV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	complementoDe2 IS
    PORT (
	OVF : OUT std_logic;
	OP : IN std_logic;
	bitA0 : IN std_logic;
	bitB0 : IN std_logic;
	bitA1 : IN std_logic;
	bitB1 : IN std_logic;
	bitA2 : IN std_logic;
	bitB2 : IN std_logic;
	bitA3 : IN std_logic;
	bitB3 : IN std_logic;
	bitA4 : IN std_logic;
	bitB4 : IN std_logic;
	RES_SINAL : OUT std_logic;
	A_SINAL : OUT std_logic;
	A_a : OUT std_logic;
	A_b : OUT std_logic;
	A_c : OUT std_logic;
	A_d : OUT std_logic;
	A_e : OUT std_logic;
	A_f : OUT std_logic;
	A_g : OUT std_logic;
	B_a : OUT std_logic;
	B_b : OUT std_logic;
	B_c : OUT std_logic;
	B_d : OUT std_logic;
	B_e : OUT std_logic;
	B_f : OUT std_logic;
	B_g : OUT std_logic;
	S_a : OUT std_logic;
	S_b : OUT std_logic;
	S_c : OUT std_logic;
	S_d : OUT std_logic;
	S_e : OUT std_logic;
	S_f : OUT std_logic;
	S_g : OUT std_logic;
	OP_INDICADOR : OUT std_logic;
	DEBUG_LED : OUT std_logic;
	B_SINAL : OUT std_logic
	);
END complementoDe2;

-- Design Ports Information
-- OVF	=>  Location: PIN_L1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RES_SINAL	=>  Location: PIN_U22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A_SINAL	=>  Location: PIN_W19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A_a	=>  Location: PIN_U20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A_b	=>  Location: PIN_Y20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A_c	=>  Location: PIN_V20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A_d	=>  Location: PIN_U16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A_e	=>  Location: PIN_U15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A_f	=>  Location: PIN_Y15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A_g	=>  Location: PIN_P9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B_a	=>  Location: PIN_Y19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B_b	=>  Location: PIN_AB17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B_c	=>  Location: PIN_AA10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B_d	=>  Location: PIN_Y14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B_e	=>  Location: PIN_V14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B_f	=>  Location: PIN_AB22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B_g	=>  Location: PIN_AB21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S_a	=>  Location: PIN_U21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S_b	=>  Location: PIN_V21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S_c	=>  Location: PIN_W22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S_d	=>  Location: PIN_W21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S_e	=>  Location: PIN_Y22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S_f	=>  Location: PIN_Y21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S_g	=>  Location: PIN_AA22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OP_INDICADOR	=>  Location: PIN_AA2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DEBUG_LED	=>  Location: PIN_AA1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B_SINAL	=>  Location: PIN_V19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OP	=>  Location: PIN_U7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- bitA1	=>  Location: PIN_AA14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- bitB1	=>  Location: PIN_V13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- bitA0	=>  Location: PIN_AB15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- bitB0	=>  Location: PIN_U13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- bitA2	=>  Location: PIN_AA13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- bitB2	=>  Location: PIN_T13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- bitA3	=>  Location: PIN_AB13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- bitB3	=>  Location: PIN_T12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- bitA4	=>  Location: PIN_AB12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- bitB4	=>  Location: PIN_AA15,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF complementoDe2 IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_OVF : std_logic;
SIGNAL ww_OP : std_logic;
SIGNAL ww_bitA0 : std_logic;
SIGNAL ww_bitB0 : std_logic;
SIGNAL ww_bitA1 : std_logic;
SIGNAL ww_bitB1 : std_logic;
SIGNAL ww_bitA2 : std_logic;
SIGNAL ww_bitB2 : std_logic;
SIGNAL ww_bitA3 : std_logic;
SIGNAL ww_bitB3 : std_logic;
SIGNAL ww_bitA4 : std_logic;
SIGNAL ww_bitB4 : std_logic;
SIGNAL ww_RES_SINAL : std_logic;
SIGNAL ww_A_SINAL : std_logic;
SIGNAL ww_A_a : std_logic;
SIGNAL ww_A_b : std_logic;
SIGNAL ww_A_c : std_logic;
SIGNAL ww_A_d : std_logic;
SIGNAL ww_A_e : std_logic;
SIGNAL ww_A_f : std_logic;
SIGNAL ww_A_g : std_logic;
SIGNAL ww_B_a : std_logic;
SIGNAL ww_B_b : std_logic;
SIGNAL ww_B_c : std_logic;
SIGNAL ww_B_d : std_logic;
SIGNAL ww_B_e : std_logic;
SIGNAL ww_B_f : std_logic;
SIGNAL ww_B_g : std_logic;
SIGNAL ww_S_a : std_logic;
SIGNAL ww_S_b : std_logic;
SIGNAL ww_S_c : std_logic;
SIGNAL ww_S_d : std_logic;
SIGNAL ww_S_e : std_logic;
SIGNAL ww_S_f : std_logic;
SIGNAL ww_S_g : std_logic;
SIGNAL ww_OP_INDICADOR : std_logic;
SIGNAL ww_DEBUG_LED : std_logic;
SIGNAL ww_B_SINAL : std_logic;
SIGNAL \~QUARTUS_CREATED_GND~I_combout\ : std_logic;
SIGNAL \bitA4~input_o\ : std_logic;
SIGNAL \bitB4~input_o\ : std_logic;
SIGNAL \OP~input_o\ : std_logic;
SIGNAL \bitB0~input_o\ : std_logic;
SIGNAL \bitB1~input_o\ : std_logic;
SIGNAL \bitA1~input_o\ : std_logic;
SIGNAL \bitA0~input_o\ : std_logic;
SIGNAL \inst|inst2|inst2~combout\ : std_logic;
SIGNAL \bitA2~input_o\ : std_logic;
SIGNAL \bitA3~input_o\ : std_logic;
SIGNAL \bitB3~input_o\ : std_logic;
SIGNAL \inst25~combout\ : std_logic;
SIGNAL \bitB2~input_o\ : std_logic;
SIGNAL \inst26~combout\ : std_logic;
SIGNAL \inst|inst4|inst2~combout\ : std_logic;
SIGNAL \hi~combout\ : std_logic;
SIGNAL \inst|inst5|inst3|inst~combout\ : std_logic;
SIGNAL \inst|inst5|inst|inst~combout\ : std_logic;
SIGNAL \inst7|inst16~0_combout\ : std_logic;
SIGNAL \inst7|nain~0_combout\ : std_logic;
SIGNAL \inst7|inst28~0_combout\ : std_logic;
SIGNAL \inst7|junk~0_combout\ : std_logic;
SIGNAL \inst7|qrdd~0_combout\ : std_logic;
SIGNAL \inst7|inst2~0_combout\ : std_logic;
SIGNAL \inst7|inst8~combout\ : std_logic;
SIGNAL \inst6|inst16~0_combout\ : std_logic;
SIGNAL \inst6|nain~0_combout\ : std_logic;
SIGNAL \inst6|inst28~0_combout\ : std_logic;
SIGNAL \inst6|junk~0_combout\ : std_logic;
SIGNAL \inst6|qrdd~0_combout\ : std_logic;
SIGNAL \inst6|inst2~0_combout\ : std_logic;
SIGNAL \inst6|inst8~combout\ : std_logic;
SIGNAL \inst|inst|inst3|inst~combout\ : std_logic;
SIGNAL \inst29~0_combout\ : std_logic;
SIGNAL \inst29~1_combout\ : std_logic;
SIGNAL \inst31~0_combout\ : std_logic;
SIGNAL \inst29~2_combout\ : std_logic;
SIGNAL \inst17|inst4|inst|inst~0_combout\ : std_logic;
SIGNAL \inst4|inst16~0_combout\ : std_logic;
SIGNAL \inst4|nain~0_combout\ : std_logic;
SIGNAL \inst4|inst28~0_combout\ : std_logic;
SIGNAL \inst4|junk~0_combout\ : std_logic;
SIGNAL \inst4|qrdd~0_combout\ : std_logic;
SIGNAL \inst4|inst2~0_combout\ : std_logic;
SIGNAL \inst4|inst8~combout\ : std_logic;
SIGNAL \ALT_INV_bitB4~input_o\ : std_logic;
SIGNAL \ALT_INV_bitA4~input_o\ : std_logic;
SIGNAL \ALT_INV_bitB3~input_o\ : std_logic;
SIGNAL \ALT_INV_bitA3~input_o\ : std_logic;
SIGNAL \ALT_INV_bitB2~input_o\ : std_logic;
SIGNAL \ALT_INV_bitA2~input_o\ : std_logic;
SIGNAL \ALT_INV_bitB0~input_o\ : std_logic;
SIGNAL \ALT_INV_bitA0~input_o\ : std_logic;
SIGNAL \ALT_INV_bitB1~input_o\ : std_logic;
SIGNAL \ALT_INV_bitA1~input_o\ : std_logic;
SIGNAL \ALT_INV_OP~input_o\ : std_logic;
SIGNAL \inst4|ALT_INV_inst8~combout\ : std_logic;
SIGNAL \inst4|ALT_INV_qrdd~0_combout\ : std_logic;
SIGNAL \inst4|ALT_INV_junk~0_combout\ : std_logic;
SIGNAL \inst4|ALT_INV_inst28~0_combout\ : std_logic;
SIGNAL \inst4|ALT_INV_nain~0_combout\ : std_logic;
SIGNAL \inst4|ALT_INV_inst16~0_combout\ : std_logic;
SIGNAL \inst17|inst4|inst|ALT_INV_inst~0_combout\ : std_logic;
SIGNAL \ALT_INV_inst29~2_combout\ : std_logic;
SIGNAL \inst|inst|inst3|ALT_INV_inst~combout\ : std_logic;
SIGNAL \ALT_INV_inst31~0_combout\ : std_logic;
SIGNAL \ALT_INV_inst29~1_combout\ : std_logic;
SIGNAL \ALT_INV_inst29~0_combout\ : std_logic;
SIGNAL \inst6|ALT_INV_inst8~combout\ : std_logic;
SIGNAL \inst6|ALT_INV_qrdd~0_combout\ : std_logic;
SIGNAL \inst6|ALT_INV_junk~0_combout\ : std_logic;
SIGNAL \inst6|ALT_INV_inst28~0_combout\ : std_logic;
SIGNAL \inst6|ALT_INV_nain~0_combout\ : std_logic;
SIGNAL \inst6|ALT_INV_inst16~0_combout\ : std_logic;
SIGNAL \inst7|ALT_INV_inst8~combout\ : std_logic;
SIGNAL \inst7|ALT_INV_qrdd~0_combout\ : std_logic;
SIGNAL \inst7|ALT_INV_junk~0_combout\ : std_logic;
SIGNAL \inst7|ALT_INV_inst28~0_combout\ : std_logic;
SIGNAL \inst7|ALT_INV_nain~0_combout\ : std_logic;
SIGNAL \inst7|ALT_INV_inst16~0_combout\ : std_logic;
SIGNAL \inst|inst5|inst|ALT_INV_inst~combout\ : std_logic;
SIGNAL \inst|inst5|inst3|ALT_INV_inst~combout\ : std_logic;
SIGNAL \inst|inst4|ALT_INV_inst2~combout\ : std_logic;
SIGNAL \ALT_INV_inst25~combout\ : std_logic;
SIGNAL \ALT_INV_inst26~combout\ : std_logic;
SIGNAL \inst|inst2|ALT_INV_inst2~combout\ : std_logic;

BEGIN

OVF <= ww_OVF;
ww_OP <= OP;
ww_bitA0 <= bitA0;
ww_bitB0 <= bitB0;
ww_bitA1 <= bitA1;
ww_bitB1 <= bitB1;
ww_bitA2 <= bitA2;
ww_bitB2 <= bitB2;
ww_bitA3 <= bitA3;
ww_bitB3 <= bitB3;
ww_bitA4 <= bitA4;
ww_bitB4 <= bitB4;
RES_SINAL <= ww_RES_SINAL;
A_SINAL <= ww_A_SINAL;
A_a <= ww_A_a;
A_b <= ww_A_b;
A_c <= ww_A_c;
A_d <= ww_A_d;
A_e <= ww_A_e;
A_f <= ww_A_f;
A_g <= ww_A_g;
B_a <= ww_B_a;
B_b <= ww_B_b;
B_c <= ww_B_c;
B_d <= ww_B_d;
B_e <= ww_B_e;
B_f <= ww_B_f;
B_g <= ww_B_g;
S_a <= ww_S_a;
S_b <= ww_S_b;
S_c <= ww_S_c;
S_d <= ww_S_d;
S_e <= ww_S_e;
S_f <= ww_S_f;
S_g <= ww_S_g;
OP_INDICADOR <= ww_OP_INDICADOR;
DEBUG_LED <= ww_DEBUG_LED;
B_SINAL <= ww_B_SINAL;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
\ALT_INV_bitB4~input_o\ <= NOT \bitB4~input_o\;
\ALT_INV_bitA4~input_o\ <= NOT \bitA4~input_o\;
\ALT_INV_bitB3~input_o\ <= NOT \bitB3~input_o\;
\ALT_INV_bitA3~input_o\ <= NOT \bitA3~input_o\;
\ALT_INV_bitB2~input_o\ <= NOT \bitB2~input_o\;
\ALT_INV_bitA2~input_o\ <= NOT \bitA2~input_o\;
\ALT_INV_bitB0~input_o\ <= NOT \bitB0~input_o\;
\ALT_INV_bitA0~input_o\ <= NOT \bitA0~input_o\;
\ALT_INV_bitB1~input_o\ <= NOT \bitB1~input_o\;
\ALT_INV_bitA1~input_o\ <= NOT \bitA1~input_o\;
\ALT_INV_OP~input_o\ <= NOT \OP~input_o\;
\inst4|ALT_INV_inst8~combout\ <= NOT \inst4|inst8~combout\;
\inst4|ALT_INV_qrdd~0_combout\ <= NOT \inst4|qrdd~0_combout\;
\inst4|ALT_INV_junk~0_combout\ <= NOT \inst4|junk~0_combout\;
\inst4|ALT_INV_inst28~0_combout\ <= NOT \inst4|inst28~0_combout\;
\inst4|ALT_INV_nain~0_combout\ <= NOT \inst4|nain~0_combout\;
\inst4|ALT_INV_inst16~0_combout\ <= NOT \inst4|inst16~0_combout\;
\inst17|inst4|inst|ALT_INV_inst~0_combout\ <= NOT \inst17|inst4|inst|inst~0_combout\;
\ALT_INV_inst29~2_combout\ <= NOT \inst29~2_combout\;
\inst|inst|inst3|ALT_INV_inst~combout\ <= NOT \inst|inst|inst3|inst~combout\;
\ALT_INV_inst31~0_combout\ <= NOT \inst31~0_combout\;
\ALT_INV_inst29~1_combout\ <= NOT \inst29~1_combout\;
\ALT_INV_inst29~0_combout\ <= NOT \inst29~0_combout\;
\inst6|ALT_INV_inst8~combout\ <= NOT \inst6|inst8~combout\;
\inst6|ALT_INV_qrdd~0_combout\ <= NOT \inst6|qrdd~0_combout\;
\inst6|ALT_INV_junk~0_combout\ <= NOT \inst6|junk~0_combout\;
\inst6|ALT_INV_inst28~0_combout\ <= NOT \inst6|inst28~0_combout\;
\inst6|ALT_INV_nain~0_combout\ <= NOT \inst6|nain~0_combout\;
\inst6|ALT_INV_inst16~0_combout\ <= NOT \inst6|inst16~0_combout\;
\inst7|ALT_INV_inst8~combout\ <= NOT \inst7|inst8~combout\;
\inst7|ALT_INV_qrdd~0_combout\ <= NOT \inst7|qrdd~0_combout\;
\inst7|ALT_INV_junk~0_combout\ <= NOT \inst7|junk~0_combout\;
\inst7|ALT_INV_inst28~0_combout\ <= NOT \inst7|inst28~0_combout\;
\inst7|ALT_INV_nain~0_combout\ <= NOT \inst7|nain~0_combout\;
\inst7|ALT_INV_inst16~0_combout\ <= NOT \inst7|inst16~0_combout\;
\inst|inst5|inst|ALT_INV_inst~combout\ <= NOT \inst|inst5|inst|inst~combout\;
\inst|inst5|inst3|ALT_INV_inst~combout\ <= NOT \inst|inst5|inst3|inst~combout\;
\inst|inst4|ALT_INV_inst2~combout\ <= NOT \inst|inst4|inst2~combout\;
\ALT_INV_inst25~combout\ <= NOT \inst25~combout\;
\ALT_INV_inst26~combout\ <= NOT \inst26~combout\;
\inst|inst2|ALT_INV_inst2~combout\ <= NOT \inst|inst2|inst2~combout\;

-- Location: IOOBUF_X0_Y20_N56
\OVF~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \hi~combout\,
	devoe => ww_devoe,
	o => ww_OVF);

-- Location: IOOBUF_X51_Y0_N53
\RES_SINAL~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|inst5|inst|inst~combout\,
	devoe => ww_devoe,
	o => ww_RES_SINAL);

-- Location: IOOBUF_X44_Y0_N2
\A_SINAL~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \ALT_INV_bitA4~input_o\,
	devoe => ww_devoe,
	o => ww_A_SINAL);

-- Location: IOOBUF_X52_Y0_N36
\A_a~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|ALT_INV_inst16~0_combout\,
	devoe => ww_devoe,
	o => ww_A_a);

-- Location: IOOBUF_X48_Y0_N59
\A_b~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|ALT_INV_nain~0_combout\,
	devoe => ww_devoe,
	o => ww_A_b);

-- Location: IOOBUF_X44_Y0_N19
\A_c~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|ALT_INV_inst28~0_combout\,
	devoe => ww_devoe,
	o => ww_A_c);

-- Location: IOOBUF_X52_Y0_N19
\A_d~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|ALT_INV_junk~0_combout\,
	devoe => ww_devoe,
	o => ww_A_d);

-- Location: IOOBUF_X43_Y0_N2
\A_e~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|ALT_INV_qrdd~0_combout\,
	devoe => ww_devoe,
	o => ww_A_e);

-- Location: IOOBUF_X36_Y0_N2
\A_f~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|inst2~0_combout\,
	devoe => ww_devoe,
	o => ww_A_f);

-- Location: IOOBUF_X29_Y0_N19
\A_g~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|ALT_INV_inst8~combout\,
	devoe => ww_devoe,
	o => ww_A_g);

-- Location: IOOBUF_X48_Y0_N42
\B_a~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst6|ALT_INV_inst16~0_combout\,
	devoe => ww_devoe,
	o => ww_B_a);

-- Location: IOOBUF_X38_Y0_N53
\B_b~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst6|ALT_INV_nain~0_combout\,
	devoe => ww_devoe,
	o => ww_B_b);

-- Location: IOOBUF_X22_Y0_N53
\B_c~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst6|ALT_INV_inst28~0_combout\,
	devoe => ww_devoe,
	o => ww_B_c);

-- Location: IOOBUF_X36_Y0_N19
\B_d~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst6|ALT_INV_junk~0_combout\,
	devoe => ww_devoe,
	o => ww_B_d);

-- Location: IOOBUF_X38_Y0_N19
\B_e~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst6|ALT_INV_qrdd~0_combout\,
	devoe => ww_devoe,
	o => ww_B_e);

-- Location: IOOBUF_X46_Y0_N53
\B_f~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst6|inst2~0_combout\,
	devoe => ww_devoe,
	o => ww_B_f);

-- Location: IOOBUF_X40_Y0_N76
\B_g~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst6|ALT_INV_inst8~combout\,
	devoe => ww_devoe,
	o => ww_B_g);

-- Location: IOOBUF_X52_Y0_N53
\S_a~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|ALT_INV_inst16~0_combout\,
	devoe => ww_devoe,
	o => ww_S_a);

-- Location: IOOBUF_X51_Y0_N36
\S_b~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|ALT_INV_nain~0_combout\,
	devoe => ww_devoe,
	o => ww_S_b);

-- Location: IOOBUF_X48_Y0_N76
\S_c~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|ALT_INV_inst28~0_combout\,
	devoe => ww_devoe,
	o => ww_S_c);

-- Location: IOOBUF_X50_Y0_N36
\S_d~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|ALT_INV_junk~0_combout\,
	devoe => ww_devoe,
	o => ww_S_d);

-- Location: IOOBUF_X48_Y0_N93
\S_e~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|ALT_INV_qrdd~0_combout\,
	devoe => ww_devoe,
	o => ww_S_e);

-- Location: IOOBUF_X50_Y0_N53
\S_f~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|inst2~0_combout\,
	devoe => ww_devoe,
	o => ww_S_f);

-- Location: IOOBUF_X46_Y0_N36
\S_g~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|ALT_INV_inst8~combout\,
	devoe => ww_devoe,
	o => ww_S_g);

-- Location: IOOBUF_X0_Y18_N79
\OP_INDICADOR~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \ALT_INV_OP~input_o\,
	devoe => ww_devoe,
	o => ww_OP_INDICADOR);

-- Location: IOOBUF_X0_Y18_N96
\DEBUG_LED~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_DEBUG_LED);

-- Location: IOOBUF_X51_Y0_N19
\B_SINAL~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \ALT_INV_bitB4~input_o\,
	devoe => ww_devoe,
	o => ww_B_SINAL);

-- Location: IOIBUF_X33_Y0_N75
\bitA4~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bitA4,
	o => \bitA4~input_o\);

-- Location: IOIBUF_X36_Y0_N35
\bitB4~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bitB4,
	o => \bitB4~input_o\);

-- Location: IOIBUF_X10_Y0_N92
\OP~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_OP,
	o => \OP~input_o\);

-- Location: IOIBUF_X33_Y0_N41
\bitB0~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bitB0,
	o => \bitB0~input_o\);

-- Location: IOIBUF_X33_Y0_N58
\bitB1~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bitB1,
	o => \bitB1~input_o\);

-- Location: IOIBUF_X34_Y0_N52
\bitA1~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bitA1,
	o => \bitA1~input_o\);

-- Location: IOIBUF_X36_Y0_N52
\bitA0~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bitA0,
	o => \bitA0~input_o\);

-- Location: LABCELL_X41_Y1_N0
\inst|inst2|inst2\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|inst2|inst2~combout\ = ( \bitA0~input_o\ & ( (!\bitB0~input_o\ & ((!\bitB1~input_o\ & (!\OP~input_o\)) # (\bitB1~input_o\ & ((\bitA1~input_o\))))) # (\bitB0~input_o\ & ((!\bitB1~input_o\ $ (\OP~input_o\)) # (\bitA1~input_o\))) ) ) # ( 
-- !\bitA0~input_o\ & ( (!\bitB0~input_o\ & ((!\bitB1~input_o\ & (!\OP~input_o\)) # (\bitB1~input_o\ & ((\bitA1~input_o\))))) # (\bitB0~input_o\ & (\bitA1~input_o\ & (!\bitB1~input_o\ $ (\OP~input_o\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000011100011100000001110001111000001111101111100000111110111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_bitB0~input_o\,
	datab => \ALT_INV_bitB1~input_o\,
	datac => \ALT_INV_OP~input_o\,
	datad => \ALT_INV_bitA1~input_o\,
	dataf => \ALT_INV_bitA0~input_o\,
	combout => \inst|inst2|inst2~combout\);

-- Location: IOIBUF_X34_Y0_N35
\bitA2~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bitA2,
	o => \bitA2~input_o\);

-- Location: IOIBUF_X33_Y0_N92
\bitA3~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bitA3,
	o => \bitA3~input_o\);

-- Location: IOIBUF_X34_Y0_N18
\bitB3~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bitB3,
	o => \bitB3~input_o\);

-- Location: LABCELL_X41_Y1_N6
inst25 : cyclonev_lcell_comb
-- Equation(s):
-- \inst25~combout\ = ( \bitB3~input_o\ & ( !\OP~input_o\ ) ) # ( !\bitB3~input_o\ & ( \OP~input_o\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010110101010101010101010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_OP~input_o\,
	dataf => \ALT_INV_bitB3~input_o\,
	combout => \inst25~combout\);

-- Location: IOIBUF_X34_Y0_N1
\bitB2~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bitB2,
	o => \bitB2~input_o\);

-- Location: LABCELL_X41_Y1_N9
inst26 : cyclonev_lcell_comb
-- Equation(s):
-- \inst26~combout\ = ( \bitB2~input_o\ & ( !\OP~input_o\ ) ) # ( !\bitB2~input_o\ & ( \OP~input_o\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010110101010101010101010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_OP~input_o\,
	dataf => \ALT_INV_bitB2~input_o\,
	combout => \inst26~combout\);

-- Location: LABCELL_X41_Y1_N15
\inst|inst4|inst2\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|inst4|inst2~combout\ = ( \inst26~combout\ & ( (!\bitA3~input_o\ & (\inst|inst2|inst2~combout\ & (\bitA2~input_o\ & !\inst25~combout\))) # (\bitA3~input_o\ & ((!\inst25~combout\) # ((\inst|inst2|inst2~combout\ & \bitA2~input_o\)))) ) ) # ( 
-- !\inst26~combout\ & ( (!\bitA3~input_o\ & (!\inst25~combout\ & ((\bitA2~input_o\) # (\inst|inst2|inst2~combout\)))) # (\bitA3~input_o\ & (((!\inst25~combout\) # (\bitA2~input_o\)) # (\inst|inst2|inst2~combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0111111100000111011111110000011100011111000000010001111100000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|inst2|ALT_INV_inst2~combout\,
	datab => \ALT_INV_bitA2~input_o\,
	datac => \ALT_INV_bitA3~input_o\,
	datad => \ALT_INV_inst25~combout\,
	dataf => \ALT_INV_inst26~combout\,
	combout => \inst|inst4|inst2~combout\);

-- Location: LABCELL_X41_Y1_N51
hi : cyclonev_lcell_comb
-- Equation(s):
-- \hi~combout\ = ( \inst|inst4|inst2~combout\ & ( (!\bitA4~input_o\ & (!\bitB4~input_o\ $ (!\OP~input_o\))) ) ) # ( !\inst|inst4|inst2~combout\ & ( (\bitA4~input_o\ & (!\bitB4~input_o\ $ (\OP~input_o\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011000000000011001100000000001100001100110000000000110011000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_bitA4~input_o\,
	datac => \ALT_INV_bitB4~input_o\,
	datad => \ALT_INV_OP~input_o\,
	dataf => \inst|inst4|ALT_INV_inst2~combout\,
	combout => \hi~combout\);

-- Location: LABCELL_X41_Y1_N57
\inst|inst5|inst3|inst\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|inst5|inst3|inst~combout\ = ( \bitB4~input_o\ & ( !\bitA4~input_o\ $ (\OP~input_o\) ) ) # ( !\bitB4~input_o\ & ( !\bitA4~input_o\ $ (!\OP~input_o\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001111001100001100111100110011001100001100111100110000110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_bitA4~input_o\,
	datad => \ALT_INV_OP~input_o\,
	dataf => \ALT_INV_bitB4~input_o\,
	combout => \inst|inst5|inst3|inst~combout\);

-- Location: LABCELL_X41_Y1_N33
\inst|inst5|inst|inst\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|inst5|inst|inst~combout\ = ( \inst25~combout\ & ( \inst|inst5|inst3|inst~combout\ & ( (!\bitA3~input_o\) # ((!\inst|inst2|inst2~combout\ & ((!\bitA2~input_o\) # (\inst26~combout\))) # (\inst|inst2|inst2~combout\ & (\inst26~combout\ & 
-- !\bitA2~input_o\))) ) ) ) # ( !\inst25~combout\ & ( \inst|inst5|inst3|inst~combout\ & ( (!\bitA3~input_o\ & ((!\inst|inst2|inst2~combout\ & ((!\bitA2~input_o\) # (\inst26~combout\))) # (\inst|inst2|inst2~combout\ & (\inst26~combout\ & !\bitA2~input_o\)))) 
-- ) ) ) # ( \inst25~combout\ & ( !\inst|inst5|inst3|inst~combout\ & ( (\bitA3~input_o\ & ((!\inst|inst2|inst2~combout\ & (!\inst26~combout\ & \bitA2~input_o\)) # (\inst|inst2|inst2~combout\ & ((!\inst26~combout\) # (\bitA2~input_o\))))) ) ) ) # ( 
-- !\inst25~combout\ & ( !\inst|inst5|inst3|inst~combout\ & ( ((!\inst|inst2|inst2~combout\ & (!\inst26~combout\ & \bitA2~input_o\)) # (\inst|inst2|inst2~combout\ & ((!\inst26~combout\) # (\bitA2~input_o\)))) # (\bitA3~input_o\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0111001111110111000100000011000110001100000010001110111111001110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|inst2|ALT_INV_inst2~combout\,
	datab => \ALT_INV_bitA3~input_o\,
	datac => \ALT_INV_inst26~combout\,
	datad => \ALT_INV_bitA2~input_o\,
	datae => \ALT_INV_inst25~combout\,
	dataf => \inst|inst5|inst3|ALT_INV_inst~combout\,
	combout => \inst|inst5|inst|inst~combout\);

-- Location: LABCELL_X41_Y1_N36
\inst7|inst16~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst7|inst16~0_combout\ = ( \bitA0~input_o\ & ( (!\bitA2~input_o\ & ((!\bitA4~input_o\ & (!\bitA1~input_o\ $ (!\bitA3~input_o\))) # (\bitA4~input_o\ & ((!\bitA1~input_o\) # (\bitA3~input_o\))))) # (\bitA2~input_o\ & ((!\bitA4~input_o\ & 
-- ((!\bitA3~input_o\) # (\bitA1~input_o\))) # (\bitA4~input_o\ & (!\bitA1~input_o\ $ (!\bitA3~input_o\))))) ) ) # ( !\bitA0~input_o\ & ( (!\bitA2~input_o\) # (\bitA1~input_o\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010111110101111101011111010111101101101101101100110110110110110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_bitA2~input_o\,
	datab => \ALT_INV_bitA4~input_o\,
	datac => \ALT_INV_bitA1~input_o\,
	datad => \ALT_INV_bitA3~input_o\,
	dataf => \ALT_INV_bitA0~input_o\,
	combout => \inst7|inst16~0_combout\);

-- Location: LABCELL_X41_Y1_N39
\inst7|nain~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst7|nain~0_combout\ = ( \bitA0~input_o\ & ( (!\bitA3~input_o\ & (((!\bitA2~input_o\ & !\bitA4~input_o\)) # (\bitA1~input_o\))) # (\bitA3~input_o\ & ((!\bitA1~input_o\) # ((\bitA2~input_o\ & \bitA4~input_o\)))) ) ) # ( !\bitA0~input_o\ & ( 
-- (!\bitA2~input_o\ & ((!\bitA4~input_o\) # ((!\bitA1~input_o\)))) # (\bitA2~input_o\ & (!\bitA4~input_o\ $ (((\bitA1~input_o\) # (\bitA3~input_o\))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1110101110011001111010111001100110001111111100011000111111110001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_bitA2~input_o\,
	datab => \ALT_INV_bitA4~input_o\,
	datac => \ALT_INV_bitA3~input_o\,
	datad => \ALT_INV_bitA1~input_o\,
	dataf => \ALT_INV_bitA0~input_o\,
	combout => \inst7|nain~0_combout\);

-- Location: LABCELL_X41_Y1_N42
\inst7|inst28~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst7|inst28~0_combout\ = ( \bitA0~input_o\ & ( (!\bitA2~input_o\ & ((!\bitA4~input_o\) # ((\bitA3~input_o\) # (\bitA1~input_o\)))) # (\bitA2~input_o\ & (((!\bitA1~input_o\) # (!\bitA3~input_o\)) # (\bitA4~input_o\))) ) ) # ( !\bitA0~input_o\ & ( 
-- (!\bitA2~input_o\ & (((!\bitA1~input_o\) # (\bitA3~input_o\)))) # (\bitA2~input_o\ & (!\bitA3~input_o\ $ (((\bitA4~input_o\ & !\bitA1~input_o\))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1110010110111010111001011011101011011111111110111101111111111011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_bitA2~input_o\,
	datab => \ALT_INV_bitA4~input_o\,
	datac => \ALT_INV_bitA1~input_o\,
	datad => \ALT_INV_bitA3~input_o\,
	dataf => \ALT_INV_bitA0~input_o\,
	combout => \inst7|inst28~0_combout\);

-- Location: LABCELL_X41_Y1_N45
\inst7|junk~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst7|junk~0_combout\ = ( \bitA0~input_o\ & ( (!\bitA2~input_o\ & (((!\bitA4~input_o\ & \bitA3~input_o\)) # (\bitA1~input_o\))) # (\bitA2~input_o\ & ((!\bitA1~input_o\) # ((\bitA4~input_o\ & !\bitA3~input_o\)))) ) ) # ( !\bitA0~input_o\ & ( 
-- (!\bitA2~input_o\ & (((!\bitA1~input_o\) # (!\bitA3~input_o\)) # (\bitA4~input_o\))) # (\bitA2~input_o\ & ((!\bitA4~input_o\ & ((\bitA3~input_o\) # (\bitA1~input_o\))) # (\bitA4~input_o\ & (!\bitA1~input_o\ $ (\bitA3~input_o\))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1011111011100111101111101110011101011011110110100101101111011010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_bitA2~input_o\,
	datab => \ALT_INV_bitA4~input_o\,
	datac => \ALT_INV_bitA1~input_o\,
	datad => \ALT_INV_bitA3~input_o\,
	dataf => \ALT_INV_bitA0~input_o\,
	combout => \inst7|junk~0_combout\);

-- Location: LABCELL_X41_Y1_N18
\inst7|qrdd~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst7|qrdd~0_combout\ = ( \bitA0~input_o\ & ( (!\bitA4~input_o\ & (\bitA3~input_o\ & ((\bitA1~input_o\) # (\bitA2~input_o\)))) # (\bitA4~input_o\ & (!\bitA3~input_o\ & ((!\bitA2~input_o\) # (!\bitA1~input_o\)))) ) ) # ( !\bitA0~input_o\ & ( 
-- (!\bitA2~input_o\) # ((!\bitA4~input_o\ $ (!\bitA3~input_o\)) # (\bitA1~input_o\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1011111111101111101111111110111100110010010011000011001001001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_bitA2~input_o\,
	datab => \ALT_INV_bitA4~input_o\,
	datac => \ALT_INV_bitA1~input_o\,
	datad => \ALT_INV_bitA3~input_o\,
	dataf => \ALT_INV_bitA0~input_o\,
	combout => \inst7|qrdd~0_combout\);

-- Location: LABCELL_X41_Y1_N21
\inst7|inst2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst7|inst2~0_combout\ = ( \bitA0~input_o\ & ( !\bitA3~input_o\ $ (((!\bitA2~input_o\ & (\bitA4~input_o\ & !\bitA1~input_o\)) # (\bitA2~input_o\ & ((!\bitA1~input_o\) # (\bitA4~input_o\))))) ) ) # ( !\bitA0~input_o\ & ( (!\bitA2~input_o\ & 
-- (!\bitA4~input_o\ & (!\bitA3~input_o\ & \bitA1~input_o\))) # (\bitA2~input_o\ & ((!\bitA4~input_o\ & (\bitA3~input_o\ & !\bitA1~input_o\)) # (\bitA4~input_o\ & (!\bitA3~input_o\ $ (\bitA1~input_o\))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001010010000001000101001000000110000111111000011000011111100001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_bitA2~input_o\,
	datab => \ALT_INV_bitA4~input_o\,
	datac => \ALT_INV_bitA3~input_o\,
	datad => \ALT_INV_bitA1~input_o\,
	dataf => \ALT_INV_bitA0~input_o\,
	combout => \inst7|inst2~0_combout\);

-- Location: LABCELL_X41_Y1_N54
\inst7|inst8\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst7|inst8~combout\ = ( \bitA0~input_o\ & ( (!\bitA1~input_o\ & ((!\bitA4~input_o\ $ (!\bitA3~input_o\)) # (\bitA2~input_o\))) # (\bitA1~input_o\ & ((!\bitA2~input_o\) # (!\bitA4~input_o\ $ (!\bitA3~input_o\)))) ) ) # ( !\bitA0~input_o\ & ( 
-- ((\bitA3~input_o\) # (\bitA2~input_o\)) # (\bitA1~input_o\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101111111111111010111111111111101111011110111100111101111011110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_bitA1~input_o\,
	datab => \ALT_INV_bitA4~input_o\,
	datac => \ALT_INV_bitA2~input_o\,
	datad => \ALT_INV_bitA3~input_o\,
	dataf => \ALT_INV_bitA0~input_o\,
	combout => \inst7|inst8~combout\);

-- Location: LABCELL_X40_Y1_N33
\inst6|inst16~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst6|inst16~0_combout\ = ( \bitB4~input_o\ & ( (!\bitB2~input_o\ & (((!\bitB1~input_o\) # (!\bitB0~input_o\)) # (\bitB3~input_o\))) # (\bitB2~input_o\ & (!\bitB1~input_o\ $ (((!\bitB3~input_o\) # (!\bitB0~input_o\))))) ) ) # ( !\bitB4~input_o\ & ( 
-- (!\bitB2~input_o\ & ((!\bitB0~input_o\) # (!\bitB3~input_o\ $ (!\bitB1~input_o\)))) # (\bitB2~input_o\ & (((!\bitB3~input_o\ & \bitB0~input_o\)) # (\bitB1~input_o\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100111101101011110011110110101111001111110101101100111111010110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_bitB3~input_o\,
	datab => \ALT_INV_bitB2~input_o\,
	datac => \ALT_INV_bitB1~input_o\,
	datad => \ALT_INV_bitB0~input_o\,
	dataf => \ALT_INV_bitB4~input_o\,
	combout => \inst6|inst16~0_combout\);

-- Location: LABCELL_X40_Y1_N30
\inst6|nain~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst6|nain~0_combout\ = ( \bitB4~input_o\ & ( (!\bitB3~input_o\ & (!\bitB1~input_o\ $ (((\bitB0~input_o\) # (\bitB2~input_o\))))) # (\bitB3~input_o\ & (((!\bitB1~input_o\)) # (\bitB2~input_o\))) ) ) # ( !\bitB4~input_o\ & ( (!\bitB3~input_o\ & 
-- ((!\bitB2~input_o\) # (!\bitB0~input_o\ $ (\bitB1~input_o\)))) # (\bitB3~input_o\ & ((!\bitB0~input_o\ & (!\bitB2~input_o\)) # (\bitB0~input_o\ & ((!\bitB1~input_o\))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1110110111001010111011011100101011010101001110111101010100111011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_bitB3~input_o\,
	datab => \ALT_INV_bitB2~input_o\,
	datac => \ALT_INV_bitB0~input_o\,
	datad => \ALT_INV_bitB1~input_o\,
	dataf => \ALT_INV_bitB4~input_o\,
	combout => \inst6|nain~0_combout\);

-- Location: LABCELL_X40_Y1_N36
\inst6|inst28~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst6|inst28~0_combout\ = ( \bitB4~input_o\ & ( (!\bitB2~input_o\ & ((!\bitB0~input_o\ $ (\bitB1~input_o\)) # (\bitB3~input_o\))) # (\bitB2~input_o\ & ((!\bitB3~input_o\ $ (!\bitB1~input_o\)) # (\bitB0~input_o\))) ) ) # ( !\bitB4~input_o\ & ( 
-- (!\bitB3~input_o\ & (((!\bitB1~input_o\) # (\bitB0~input_o\)) # (\bitB2~input_o\))) # (\bitB3~input_o\ & ((!\bitB2~input_o\) # ((\bitB0~input_o\ & !\bitB1~input_o\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1110111101101110111011110110111011010111011011111101011101101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_bitB3~input_o\,
	datab => \ALT_INV_bitB2~input_o\,
	datac => \ALT_INV_bitB0~input_o\,
	datad => \ALT_INV_bitB1~input_o\,
	dataf => \ALT_INV_bitB4~input_o\,
	combout => \inst6|inst28~0_combout\);

-- Location: LABCELL_X40_Y1_N39
\inst6|junk~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst6|junk~0_combout\ = ( \bitB4~input_o\ & ( (!\bitB2~input_o\ & (((!\bitB0~input_o\) # (\bitB1~input_o\)))) # (\bitB2~input_o\ & ((!\bitB3~input_o\ & ((!\bitB1~input_o\) # (\bitB0~input_o\))) # (\bitB3~input_o\ & (!\bitB1~input_o\ $ 
-- (!\bitB0~input_o\))))) ) ) # ( !\bitB4~input_o\ & ( (!\bitB1~input_o\ & ((!\bitB2~input_o\ $ (\bitB0~input_o\)) # (\bitB3~input_o\))) # (\bitB1~input_o\ & ((!\bitB2~input_o\ & ((!\bitB3~input_o\) # (\bitB0~input_o\))) # (\bitB2~input_o\ & 
-- ((!\bitB0~input_o\))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1101101101111100110110110111110011101101001111101110110100111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_bitB3~input_o\,
	datab => \ALT_INV_bitB2~input_o\,
	datac => \ALT_INV_bitB1~input_o\,
	datad => \ALT_INV_bitB0~input_o\,
	dataf => \ALT_INV_bitB4~input_o\,
	combout => \inst6|junk~0_combout\);

-- Location: LABCELL_X40_Y1_N42
\inst6|qrdd~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst6|qrdd~0_combout\ = ( \bitB4~input_o\ & ( (!\bitB2~input_o\ & ((!\bitB3~input_o\) # ((!\bitB0~input_o\)))) # (\bitB2~input_o\ & ((!\bitB1~input_o\ & (!\bitB3~input_o\)) # (\bitB1~input_o\ & ((!\bitB0~input_o\))))) ) ) # ( !\bitB4~input_o\ & ( 
-- (!\bitB1~input_o\ & ((!\bitB2~input_o\ & ((!\bitB0~input_o\))) # (\bitB2~input_o\ & (\bitB3~input_o\)))) # (\bitB1~input_o\ & (((!\bitB0~input_o\)) # (\bitB3~input_o\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1101111100010101110111110001010111101111101010001110111110101000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_bitB3~input_o\,
	datab => \ALT_INV_bitB2~input_o\,
	datac => \ALT_INV_bitB1~input_o\,
	datad => \ALT_INV_bitB0~input_o\,
	dataf => \ALT_INV_bitB4~input_o\,
	combout => \inst6|qrdd~0_combout\);

-- Location: LABCELL_X40_Y1_N45
\inst6|inst2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst6|inst2~0_combout\ = ( \bitB4~input_o\ & ( (!\bitB2~input_o\ & (\bitB0~input_o\ & (!\bitB3~input_o\ $ (!\bitB1~input_o\)))) # (\bitB2~input_o\ & (!\bitB3~input_o\ $ (((\bitB1~input_o\) # (\bitB0~input_o\))))) ) ) # ( !\bitB4~input_o\ & ( 
-- (!\bitB3~input_o\ & ((!\bitB2~input_o\ & ((\bitB1~input_o\) # (\bitB0~input_o\))) # (\bitB2~input_o\ & (\bitB0~input_o\ & \bitB1~input_o\)))) # (\bitB3~input_o\ & (\bitB2~input_o\ & ((!\bitB1~input_o\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001100110001010000110011000101000100101000110010010010100011001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_bitB3~input_o\,
	datab => \ALT_INV_bitB2~input_o\,
	datac => \ALT_INV_bitB0~input_o\,
	datad => \ALT_INV_bitB1~input_o\,
	dataf => \ALT_INV_bitB4~input_o\,
	combout => \inst6|inst2~0_combout\);

-- Location: LABCELL_X40_Y1_N48
\inst6|inst8\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst6|inst8~combout\ = ( \bitB4~input_o\ & ( (!\bitB3~input_o\ & (((\bitB2~input_o\) # (\bitB0~input_o\)) # (\bitB1~input_o\))) # (\bitB3~input_o\ & ((!\bitB0~input_o\) # (!\bitB1~input_o\ $ (!\bitB2~input_o\)))) ) ) # ( !\bitB4~input_o\ & ( 
-- ((!\bitB1~input_o\ & ((\bitB2~input_o\))) # (\bitB1~input_o\ & ((!\bitB0~input_o\) # (!\bitB2~input_o\)))) # (\bitB3~input_o\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0111011111111101011101111111110101111011111111100111101111111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_bitB3~input_o\,
	datab => \ALT_INV_bitB1~input_o\,
	datac => \ALT_INV_bitB0~input_o\,
	datad => \ALT_INV_bitB2~input_o\,
	dataf => \ALT_INV_bitB4~input_o\,
	combout => \inst6|inst8~combout\);

-- Location: LABCELL_X41_Y1_N24
\inst|inst|inst3|inst\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|inst|inst3|inst~combout\ = ( \bitA0~input_o\ & ( !\bitB0~input_o\ ) ) # ( !\bitA0~input_o\ & ( \bitB0~input_o\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111111110000111100001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_bitB0~input_o\,
	dataf => \ALT_INV_bitA0~input_o\,
	combout => \inst|inst|inst3|inst~combout\);

-- Location: LABCELL_X41_Y1_N48
\inst29~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst29~0_combout\ = ( \bitB4~input_o\ & ( !\bitA4~input_o\ ) ) # ( !\bitB4~input_o\ & ( \bitA4~input_o\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001111001100110011001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_bitA4~input_o\,
	dataf => \ALT_INV_bitB4~input_o\,
	combout => \inst29~0_combout\);

-- Location: LABCELL_X41_Y1_N30
\inst29~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst29~1_combout\ = ( \inst25~combout\ & ( \inst29~0_combout\ & ( (!\bitA3~input_o\) # ((!\inst|inst2|inst2~combout\ & ((!\bitA2~input_o\) # (\inst26~combout\))) # (\inst|inst2|inst2~combout\ & (!\bitA2~input_o\ & \inst26~combout\))) ) ) ) # ( 
-- !\inst25~combout\ & ( \inst29~0_combout\ & ( (!\bitA3~input_o\ & ((!\inst|inst2|inst2~combout\ & ((!\bitA2~input_o\) # (\inst26~combout\))) # (\inst|inst2|inst2~combout\ & (!\bitA2~input_o\ & \inst26~combout\)))) ) ) ) # ( \inst25~combout\ & ( 
-- !\inst29~0_combout\ & ( (\bitA3~input_o\ & ((!\inst|inst2|inst2~combout\ & (\bitA2~input_o\ & !\inst26~combout\)) # (\inst|inst2|inst2~combout\ & ((!\inst26~combout\) # (\bitA2~input_o\))))) ) ) ) # ( !\inst25~combout\ & ( !\inst29~0_combout\ & ( 
-- ((!\inst|inst2|inst2~combout\ & (\bitA2~input_o\ & !\inst26~combout\)) # (\inst|inst2|inst2~combout\ & ((!\inst26~combout\) # (\bitA2~input_o\)))) # (\bitA3~input_o\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0111111100110111000100110000000110000000110010001110110011111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|inst2|ALT_INV_inst2~combout\,
	datab => \ALT_INV_bitA3~input_o\,
	datac => \ALT_INV_bitA2~input_o\,
	datad => \ALT_INV_inst26~combout\,
	datae => \ALT_INV_inst25~combout\,
	dataf => \ALT_INV_inst29~0_combout\,
	combout => \inst29~1_combout\);

-- Location: LABCELL_X41_Y1_N3
\inst31~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst31~0_combout\ = ( \bitA0~input_o\ & ( !\bitB1~input_o\ $ (!\bitA1~input_o\ $ (((!\OP~input_o\) # (\bitB0~input_o\)))) ) ) # ( !\bitA0~input_o\ & ( !\bitB1~input_o\ $ (!\bitA1~input_o\ $ (((!\bitB0~input_o\ & !\OP~input_o\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1001011000111100100101100011110011000011011010011100001101101001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_bitB0~input_o\,
	datab => \ALT_INV_bitB1~input_o\,
	datac => \ALT_INV_bitA1~input_o\,
	datad => \ALT_INV_OP~input_o\,
	dataf => \ALT_INV_bitA0~input_o\,
	combout => \inst31~0_combout\);

-- Location: LABCELL_X41_Y1_N12
\inst29~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst29~2_combout\ = ( \bitA2~input_o\ & ( !\inst|inst2|inst2~combout\ $ (\bitB2~input_o\) ) ) # ( !\bitA2~input_o\ & ( !\inst|inst2|inst2~combout\ $ (!\bitB2~input_o\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101101001011010010110100101101010100101101001011010010110100101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|inst2|ALT_INV_inst2~combout\,
	datac => \ALT_INV_bitB2~input_o\,
	dataf => \ALT_INV_bitA2~input_o\,
	combout => \inst29~2_combout\);

-- Location: LABCELL_X41_Y1_N27
\inst17|inst4|inst|inst~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst17|inst4|inst|inst~0_combout\ = ( \bitA2~input_o\ & ( !\bitA3~input_o\ $ (!\bitB3~input_o\ $ (((!\inst26~combout\) # (\inst|inst2|inst2~combout\)))) ) ) # ( !\bitA2~input_o\ & ( !\bitA3~input_o\ $ (!\bitB3~input_o\ $ (((\inst|inst2|inst2~combout\ & 
-- !\inst26~combout\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0110001110011100011000111001110011000110001110011100011000111001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|inst2|ALT_INV_inst2~combout\,
	datab => \ALT_INV_bitA3~input_o\,
	datac => \ALT_INV_inst26~combout\,
	datad => \ALT_INV_bitB3~input_o\,
	dataf => \ALT_INV_bitA2~input_o\,
	combout => \inst17|inst4|inst|inst~0_combout\);

-- Location: LABCELL_X47_Y1_N30
\inst4|inst16~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst4|inst16~0_combout\ = ( \inst29~2_combout\ & ( \inst17|inst4|inst|inst~0_combout\ & ( (!\inst|inst|inst3|inst~combout\ & ((!\inst29~1_combout\ $ (\inst|inst5|inst|inst~combout\)) # (\inst31~0_combout\))) # (\inst|inst|inst3|inst~combout\ & 
-- (!\inst29~1_combout\ $ (((!\inst31~0_combout\))))) ) ) ) # ( !\inst29~2_combout\ & ( \inst17|inst4|inst|inst~0_combout\ & ( (!\inst31~0_combout\) # (!\inst29~1_combout\ $ (((!\inst|inst5|inst|inst~combout\) # (\inst|inst|inst3|inst~combout\)))) ) ) ) # ( 
-- \inst29~2_combout\ & ( !\inst17|inst4|inst|inst~0_combout\ & ( (!\inst29~1_combout\ $ (((!\inst|inst|inst3|inst~combout\ & \inst|inst5|inst|inst~combout\)))) # (\inst31~0_combout\) ) ) ) # ( !\inst29~2_combout\ & ( !\inst17|inst4|inst|inst~0_combout\ & ( 
-- (!\inst|inst|inst3|inst~combout\ & ((!\inst31~0_combout\) # (!\inst29~1_combout\ $ (!\inst|inst5|inst|inst~combout\)))) # (\inst|inst|inst3|inst~combout\ & (!\inst29~1_combout\ $ (((!\inst31~0_combout\))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1011101101101100110001101111111111111111001110011001001111101110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|inst|inst3|ALT_INV_inst~combout\,
	datab => \ALT_INV_inst29~1_combout\,
	datac => \inst|inst5|inst|ALT_INV_inst~combout\,
	datad => \ALT_INV_inst31~0_combout\,
	datae => \ALT_INV_inst29~2_combout\,
	dataf => \inst17|inst4|inst|ALT_INV_inst~0_combout\,
	combout => \inst4|inst16~0_combout\);

-- Location: LABCELL_X47_Y1_N6
\inst4|nain~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst4|nain~0_combout\ = ( \inst29~2_combout\ & ( \inst17|inst4|inst|inst~0_combout\ & ( ((!\inst|inst|inst3|inst~combout\ & (!\inst|inst5|inst|inst~combout\ & \inst31~0_combout\)) # (\inst|inst|inst3|inst~combout\ & ((!\inst31~0_combout\)))) # 
-- (\inst29~1_combout\) ) ) ) # ( !\inst29~2_combout\ & ( \inst17|inst4|inst|inst~0_combout\ & ( (!\inst|inst|inst3|inst~combout\ & ((!\inst|inst5|inst|inst~combout\ & ((!\inst31~0_combout\))) # (\inst|inst5|inst|inst~combout\ & ((!\inst29~1_combout\) # 
-- (\inst31~0_combout\))))) # (\inst|inst|inst3|inst~combout\ & (((!\inst31~0_combout\)))) ) ) ) # ( \inst29~2_combout\ & ( !\inst17|inst4|inst|inst~0_combout\ & ( (!\inst|inst|inst3|inst~combout\ & ((!\inst|inst5|inst|inst~combout\ & ((\inst31~0_combout\))) 
-- # (\inst|inst5|inst|inst~combout\ & ((!\inst31~0_combout\) # (\inst29~1_combout\))))) # (\inst|inst|inst3|inst~combout\ & (((\inst31~0_combout\)))) ) ) ) # ( !\inst29~2_combout\ & ( !\inst17|inst4|inst|inst~0_combout\ & ( (!\inst29~1_combout\) # 
-- ((!\inst|inst|inst3|inst~combout\ & (!\inst|inst5|inst|inst~combout\ & !\inst31~0_combout\)) # (\inst|inst|inst3|inst~combout\ & ((\inst31~0_combout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1110110011011101000010101111011111111101000010100111011110110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|inst|inst3|ALT_INV_inst~combout\,
	datab => \ALT_INV_inst29~1_combout\,
	datac => \inst|inst5|inst|ALT_INV_inst~combout\,
	datad => \ALT_INV_inst31~0_combout\,
	datae => \ALT_INV_inst29~2_combout\,
	dataf => \inst17|inst4|inst|ALT_INV_inst~0_combout\,
	combout => \inst4|nain~0_combout\);

-- Location: LABCELL_X47_Y1_N42
\inst4|inst28~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst4|inst28~0_combout\ = ( \inst29~2_combout\ & ( \inst17|inst4|inst|inst~0_combout\ & ( (!\inst31~0_combout\ & (((\inst29~1_combout\ & !\inst|inst5|inst|inst~combout\)) # (\inst|inst|inst3|inst~combout\))) # (\inst31~0_combout\ & (!\inst29~1_combout\ $ 
-- (((\inst|inst5|inst|inst~combout\) # (\inst|inst|inst3|inst~combout\))))) ) ) ) # ( !\inst29~2_combout\ & ( \inst17|inst4|inst|inst~0_combout\ & ( (((!\inst31~0_combout\) # (\inst|inst5|inst|inst~combout\)) # (\inst29~1_combout\)) # 
-- (\inst|inst|inst3|inst~combout\) ) ) ) # ( \inst29~2_combout\ & ( !\inst17|inst4|inst|inst~0_combout\ & ( ((!\inst29~1_combout\) # ((\inst31~0_combout\) # (\inst|inst5|inst|inst~combout\))) # (\inst|inst|inst3|inst~combout\) ) ) ) # ( !\inst29~2_combout\ 
-- & ( !\inst17|inst4|inst|inst~0_combout\ & ( (!\inst31~0_combout\ & (!\inst29~1_combout\ $ (((!\inst|inst|inst3|inst~combout\ & !\inst|inst5|inst|inst~combout\))))) # (\inst31~0_combout\ & (((!\inst29~1_combout\ & !\inst|inst5|inst|inst~combout\)) # 
-- (\inst|inst|inst3|inst~combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0110110011010101110111111111111111111111011111110111010110010011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|inst|inst3|ALT_INV_inst~combout\,
	datab => \ALT_INV_inst29~1_combout\,
	datac => \inst|inst5|inst|ALT_INV_inst~combout\,
	datad => \ALT_INV_inst31~0_combout\,
	datae => \ALT_INV_inst29~2_combout\,
	dataf => \inst17|inst4|inst|ALT_INV_inst~0_combout\,
	combout => \inst4|inst28~0_combout\);

-- Location: LABCELL_X47_Y1_N18
\inst4|junk~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst4|junk~0_combout\ = ( \inst29~2_combout\ & ( \inst17|inst4|inst|inst~0_combout\ & ( (!\inst|inst|inst3|inst~combout\ & ((!\inst29~1_combout\) # ((\inst31~0_combout\) # (\inst|inst5|inst|inst~combout\)))) # (\inst|inst|inst3|inst~combout\ & 
-- (((!\inst31~0_combout\)))) ) ) ) # ( !\inst29~2_combout\ & ( \inst17|inst4|inst|inst~0_combout\ & ( (!\inst|inst|inst3|inst~combout\ & ((!\inst|inst5|inst|inst~combout\ & ((\inst31~0_combout\) # (\inst29~1_combout\))) # (\inst|inst5|inst|inst~combout\ & 
-- ((!\inst31~0_combout\))))) # (\inst|inst|inst3|inst~combout\ & ((!\inst29~1_combout\) # ((\inst31~0_combout\)))) ) ) ) # ( \inst29~2_combout\ & ( !\inst17|inst4|inst|inst~0_combout\ & ( (!\inst|inst|inst3|inst~combout\ & ((!\inst|inst5|inst|inst~combout\ 
-- & ((!\inst29~1_combout\) # (!\inst31~0_combout\))) # (\inst|inst5|inst|inst~combout\ & ((\inst31~0_combout\))))) # (\inst|inst|inst3|inst~combout\ & (((!\inst31~0_combout\)) # (\inst29~1_combout\))) ) ) ) # ( !\inst29~2_combout\ & ( 
-- !\inst17|inst4|inst|inst~0_combout\ & ( (!\inst|inst|inst3|inst~combout\ & (((!\inst31~0_combout\) # (\inst|inst5|inst|inst~combout\)) # (\inst29~1_combout\))) # (\inst|inst|inst3|inst~combout\ & (((\inst31~0_combout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101001111111111101011001101101101110111101011101111110101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|inst|inst3|ALT_INV_inst~combout\,
	datab => \ALT_INV_inst29~1_combout\,
	datac => \inst|inst5|inst|ALT_INV_inst~combout\,
	datad => \ALT_INV_inst31~0_combout\,
	datae => \ALT_INV_inst29~2_combout\,
	dataf => \inst17|inst4|inst|ALT_INV_inst~0_combout\,
	combout => \inst4|junk~0_combout\);

-- Location: LABCELL_X47_Y1_N24
\inst4|qrdd~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst4|qrdd~0_combout\ = ( \inst29~2_combout\ & ( \inst17|inst4|inst|inst~0_combout\ & ( (!\inst29~1_combout\) # ((!\inst|inst|inst3|inst~combout\ & ((\inst31~0_combout\) # (\inst|inst5|inst|inst~combout\)))) ) ) ) # ( !\inst29~2_combout\ & ( 
-- \inst17|inst4|inst|inst~0_combout\ & ( (!\inst31~0_combout\ & (!\inst|inst|inst3|inst~combout\)) # (\inst31~0_combout\ & ((!\inst29~1_combout\) # ((!\inst|inst|inst3|inst~combout\ & !\inst|inst5|inst|inst~combout\)))) ) ) ) # ( \inst29~2_combout\ & ( 
-- !\inst17|inst4|inst|inst~0_combout\ & ( (!\inst31~0_combout\ & (((!\inst|inst|inst3|inst~combout\ & !\inst|inst5|inst|inst~combout\)) # (\inst29~1_combout\))) # (\inst31~0_combout\ & (!\inst|inst|inst3|inst~combout\)) ) ) ) # ( !\inst29~2_combout\ & ( 
-- !\inst17|inst4|inst|inst~0_combout\ & ( ((!\inst|inst|inst3|inst~combout\ & ((!\inst31~0_combout\) # (\inst|inst5|inst|inst~combout\)))) # (\inst29~1_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1011101100111011101100111010101010101010111011001100111011101110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|inst|inst3|ALT_INV_inst~combout\,
	datab => \ALT_INV_inst29~1_combout\,
	datac => \inst|inst5|inst|ALT_INV_inst~combout\,
	datad => \ALT_INV_inst31~0_combout\,
	datae => \ALT_INV_inst29~2_combout\,
	dataf => \inst17|inst4|inst|ALT_INV_inst~0_combout\,
	combout => \inst4|qrdd~0_combout\);

-- Location: LABCELL_X47_Y1_N0
\inst4|inst2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst4|inst2~0_combout\ = ( \inst29~2_combout\ & ( \inst17|inst4|inst|inst~0_combout\ & ( (!\inst31~0_combout\ & (((\inst|inst5|inst|inst~combout\)) # (\inst|inst|inst3|inst~combout\))) # (\inst31~0_combout\ & (\inst29~1_combout\ & 
-- ((!\inst|inst5|inst|inst~combout\) # (\inst|inst|inst3|inst~combout\)))) ) ) ) # ( !\inst29~2_combout\ & ( \inst17|inst4|inst|inst~0_combout\ & ( (!\inst|inst|inst3|inst~combout\ & (!\inst29~1_combout\ & (!\inst|inst5|inst|inst~combout\ & 
-- \inst31~0_combout\))) # (\inst|inst|inst3|inst~combout\ & (\inst29~1_combout\ & ((!\inst31~0_combout\)))) ) ) ) # ( \inst29~2_combout\ & ( !\inst17|inst4|inst|inst~0_combout\ & ( (!\inst|inst|inst3|inst~combout\ & (\inst29~1_combout\ & 
-- (!\inst|inst5|inst|inst~combout\ & !\inst31~0_combout\))) # (\inst|inst|inst3|inst~combout\ & (!\inst29~1_combout\ & ((\inst31~0_combout\)))) ) ) ) # ( !\inst29~2_combout\ & ( !\inst17|inst4|inst|inst~0_combout\ & ( (!\inst31~0_combout\ & 
-- (!\inst29~1_combout\ & ((!\inst|inst5|inst|inst~combout\) # (\inst|inst|inst3|inst~combout\)))) # (\inst31~0_combout\ & (((\inst|inst5|inst|inst~combout\)) # (\inst|inst|inst3|inst~combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100010001011111001000000100010000010001100000000101111100110001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|inst|inst3|ALT_INV_inst~combout\,
	datab => \ALT_INV_inst29~1_combout\,
	datac => \inst|inst5|inst|ALT_INV_inst~combout\,
	datad => \ALT_INV_inst31~0_combout\,
	datae => \ALT_INV_inst29~2_combout\,
	dataf => \inst17|inst4|inst|ALT_INV_inst~0_combout\,
	combout => \inst4|inst2~0_combout\);

-- Location: LABCELL_X47_Y1_N36
\inst4|inst8\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst4|inst8~combout\ = ( \inst29~2_combout\ & ( \inst17|inst4|inst|inst~0_combout\ & ( (!\inst31~0_combout\) # (!\inst29~1_combout\ $ (((!\inst|inst|inst3|inst~combout\ & !\inst|inst5|inst|inst~combout\)))) ) ) ) # ( !\inst29~2_combout\ & ( 
-- \inst17|inst4|inst|inst~0_combout\ & ( (!\inst|inst|inst3|inst~combout\) # ((!\inst29~1_combout\) # (\inst31~0_combout\)) ) ) ) # ( \inst29~2_combout\ & ( !\inst17|inst4|inst|inst~0_combout\ & ( (!\inst|inst|inst3|inst~combout\) # ((!\inst31~0_combout\) # 
-- (\inst29~1_combout\)) ) ) ) # ( !\inst29~2_combout\ & ( !\inst17|inst4|inst|inst~0_combout\ & ( (!\inst29~1_combout\ $ (((\inst|inst5|inst|inst~combout\) # (\inst|inst|inst3|inst~combout\)))) # (\inst31~0_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1001001111111111111111111011101111101110111111111111111101101100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst|inst|inst3|ALT_INV_inst~combout\,
	datab => \ALT_INV_inst29~1_combout\,
	datac => \inst|inst5|inst|ALT_INV_inst~combout\,
	datad => \ALT_INV_inst31~0_combout\,
	datae => \ALT_INV_inst29~2_combout\,
	dataf => \inst17|inst4|inst|ALT_INV_inst~0_combout\,
	combout => \inst4|inst8~combout\);

-- Location: LABCELL_X39_Y40_N0
\~QUARTUS_CREATED_GND~I\ : cyclonev_lcell_comb
-- Equation(s):

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
;
END structure;


